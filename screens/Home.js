import { StatusBar } from 'expo-status-bar';
import React, { useState ,useEffect} from 'react';
import { StyleSheet, Text, View, TextInput, Button as RNButton } from 'react-native';
import {
    NativeBaseProvider, FlatList,
    Image,
    Avatar,
    HStack,
    VStack,
    Spacer,
    Box
} from 'native-base';
import { MaterialIcons } from "@expo/vector-icons"
import { StackActions } from '@react-navigation/native';
import { getAuth, signOut } from "firebase/auth";
import { getFirestore ,collection, onSnapshot} from "firebase/firestore";

const auth = getAuth();
const db = getFirestore();

export default function App({ navigation }) {
    const [data,setData] = useState([]);
    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (<>
                <RNButton onPress={() => {
                    
                    navigation.navigate('Cadet')
                }} title="Add Cadet" />
                </>
            ),
            headerLeft: () => (<>
                <RNButton onPress={() => {
                    signOut(auth).then(() => {
                        navigation.dispatch(
                            StackActions.replace('Login')
                        );
                    }).catch((error) => {
                        alert(error.message);
                    });

                }} title="Log out" />
                </>
            ),
        });
    }, [navigation]);
    useEffect(()=>{
        const unsub = onSnapshot(collection(db, "Cadet"), (snap) => {
            const temp = [];
            snap.forEach(doc => {
                console.log("Current data: ", doc.data());
                temp.push(doc.data())
            });
            setData(temp);

        });
    },[])

    return (
        <NativeBaseProvider>
            <View style={styles.container}>
                <Text style={{ color: 'orange', fontSize: 22, fontWeight: 'bold', margin: 30,}}>{'Cadet List'}</Text>
                <FlatList
                    data={data}
                    style={{width:'100%'}}
                    renderItem={({ item }) => (<CellItem item={item} key={item.id}/>)}
                    keyExtractor={(item) => item.id}
                />
            </View>
        </NativeBaseProvider>
    );
}

const CellItem = ({ item }) => {
    return (
        <Box
            borderBottomWidth="1"
            _dark={{
                borderColor: "gray.600",
            }}
            borderColor="coolGray.200"
            pl="4"
            pr="5"
            py="2"
        >
            <HStack space={4} justifyContent="space-between">
                <Avatar
                    size="48px"
                    source={{
                        uri: `https://ui-avatars.com/api/?name=${item.name}`,
                    }}
                />
                <VStack>
                    <Text
                        _dark={{
                            color: "warmGray.50",
                        }}
                        color="coolGray.800"
                        style={{fontWeight: "bold"}}
                    >
                        {item.name}
                    </Text>
                    <Text
                        color="coolGray.600"
                        _dark={{
                            color: "warmGray.200",
                        }}
                    >
                        {item.phone}
                    </Text>
                </VStack>
                <Spacer />
                <Text
                    fontSize="xs"
                    _dark={{
                        color: "warmGray.50",
                    }}
                    color="coolGray.800"
                    alignSelf="flex-start"
                >
                    {item.service}
                </Text>
            </HStack>
        </Box>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});


